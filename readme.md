Projeto em laravel 5.7 

https://medium.com/@martin.riedweg/laravel-5-7-api-authentification-with-laravel-passport-92b909e12528

- Como rodar o projeto

    após clonar o repositório executar dentro da pasta do projeto:
    
        composer update

- Passos seguidos para montar essa api de teste
    
    1 - Criar projeto laravel: 

        composer create-project --prefer-dist laravel/laravel api-authentification "5.7.*"
    
    2 - instalar Passport via composer package manager
        
        composer require laravel/passport
    
    
    O provedor de serviços Passport registra seu próprio diretório de migração de banco de dados com a estrutura,
    portanto, você deve migrar seu banco de dados depois de instalar o pacote. As migrações do Passport criarão as
    tabelas que seu aplicativo precisa para armazenar clientes e acessar tokens
    
    3 - Fazer a migração do banco
    
        php artisan migrate
    
    Em seguida, você deve executar o comando passport: install. Esse comando criará as chaves de criptografia necessárias
    para gerar tokens de acesso seguro. Além disso, o comando criará clientes de "acesso pessoal" e "concessão de senha",
    que serão usados ​​para gerar tokens de acesso
    
    4 - php artisan passport:install
    
    Depois de executar este comando, adicione o traço Laravel \ Passport \ HasApiTokens ao seu modelo App \ User.
    Esse atributo fornecerá alguns métodos auxiliares ao seu modelo, que permitem inspecionar os escopos e tokens do usuário autenticado
    
    5 - No model User adicionar o import:
        
        use Laravel\Passport\HasApiTokens;
    
    e no escopo da class User extends Authenticatable adiconar ao lado de Notifiable um HasApiTokens:
    
        <?php
        
        namespace App;
        
        use Laravel\Passport\HasApiTokens;
        use Illuminate\Notifications\Notifiable;
        use Illuminate\Foundation\Auth\User as Authenticatable;
        
        class User extends Authenticatable
        {
            use HasApiTokens, Notifiable;
        }
    
    
    6 - Ir em auth.php no diretório config e alterar o guards para utiliza o passport:
    
            'guards' => [
                'web' => [
                    'driver' => 'session',
                    'provider' => 'users',
                ],
        
                'api' => [
                    'driver' => 'passport',
                    'provider' => 'users',
                ],
            ],
    
    
    7 - Último passo configurar as rotas :)






